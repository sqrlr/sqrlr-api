package net.poundex.sqrlr.api.model.image;

import java.time.Instant;

public record ImageModel(String id, String uri, Instant created) { }
