package net.poundex.sqrlr.api.model.page;

public record CollectionModel(String id, String name) { }
