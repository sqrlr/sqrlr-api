package net.poundex.sqrlr.api.model.scan;


import javax.validation.constraints.NotBlank;

public record CreateBatchInput(@NotBlank String collectionId, @NotBlank String volume) { }
