package net.poundex.sqrlr.api.model.scan;

import net.poundex.sqrlr.api.model.page.CollectionModel;

import java.time.Instant;

public record BatchModel(
		String id, 
		CollectionModel collection, 
		String volume, 
		Instant created
) { }
